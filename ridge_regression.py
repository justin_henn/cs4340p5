
#Justin Henn
#project 5

import numpy as np
from matplotlib import pyplot as plt

#random seed for making X

np.random.seed(1)

N = 12
D_splits = 3

#X needs to be -2 through 10 uniformly
#Y needs to be X^2 + 10

X = np.random.uniform(-2,10, (N, 1))
print("X")
print(X)
print("\n")
X_scatter = X
Y = np.square(X) + 10
print("Y")
print(Y)
print("\n")
the_lambdas = [.1, 1, 10, 100]
E_cv = []

#the_ones is a matrix of ones for the bias field or w_0
#need to add that matrix to X so can get both ws

the_ones = np.ones((N, 1), dtype = int)
X = np.column_stack((the_ones, X))
X_training_sets = []
X_cv_sets = []
Y_training_sets = []
Y_cv_sets = []
w_plots = []


#linear regression

linear_w = np.linalg.inv(np.dot(X.transpose(), X))
linear_y = np.dot(X.transpose(), Y)
print("linear regression without regularization")

linear_weights = np.dot(linear_w, linear_y)
#print(linear_weights)
for i in range(0, len(linear_weights)):
    print("w%d: " % (i), end="")
    print(linear_weights[i])
print("\n")

#split datasets

X_cv = np.array_split(X, D_splits)
Y_cv = np.array_split(Y, D_splits)
X1_train = np.split(X, [8])
X1_cv = X1_train[1]
X1_train = X1_train[0]
X2_train = np.concatenate((X_cv[0], X_cv[2]))
X2_cv = X_cv[1]
X3_train = np.split(X, [4])
X3_cv = X3_train[0]
X3_train = X3_train[1]
Y1_train = np.split(Y, [8])
Y1_cv = Y1_train[1]
Y1_train = Y1_train[0]
Y2_train = np.concatenate((Y_cv[0], Y_cv[2]))
Y2_cv = Y_cv[1]
Y3_train = np.split(Y, [4])
Y3_cv = Y3_train[0]
Y3_train = Y3_train[1]

X_training_sets.append(X1_train)
X_training_sets.append(X2_train)
X_training_sets.append(X3_train)
X_cv_sets.append(X1_cv)
X_cv_sets.append(X2_cv)
X_cv_sets.append(X3_cv)
Y_training_sets.append(Y1_train)
Y_training_sets.append(Y2_train)
Y_training_sets.append(Y3_train)
Y_cv_sets.append(Y1_cv)
Y_cv_sets.append(Y2_cv)
Y_cv_sets.append(Y3_cv)

for i in range(0, len(the_lambdas)):
    e_n = 0
    Ein = []
    for z in range(0, D_splits):
        y_hat = 0
        y_cv_total = 0
        y_hat_training = 0
        y_train_total = 0
        w_reg = np.linalg.inv(np.dot(X_training_sets[z].transpose(), X_training_sets[z]) + (the_lambdas[i] * np.identity(2)))
        y_reg = np.dot(X_training_sets[z].transpose(), Y_training_sets[z])
        w = np.dot(w_reg, y_reg)
        for d in range (0, 16):
            if d % 2 == 1:
                y_hat_training += X_training_sets[z].item(d)*w[1] + w[0]
        y_train_total = np.sum(Y_training_sets[z])
        Ein.append((y_hat_training - y_train_total)**2)
        y_cv_total = np.sum(Y_cv_sets[z])
        for j in range(0, 8):
            if j % 2 == 1:
                y_hat += X_cv_sets[z].item(j)*w[1] + w[0]
        e_n += (y_cv_total - y_hat)
    E_cv.append(e_n / D_splits)
    print("Lambda:", end="")
    print(the_lambdas[i])
    print("Ein:")
    for j in range(0, 3):
        print("D-%d: " % (j+1), end="")
        print(Ein[j]/8)#[0][0])
    print("Ecv: ", end="")
    print(E_cv[i])
    
print("Choosing .1 for lambda")
w_reg = np.linalg.inv(np.dot(X.transpose(), X) + (the_lambdas[0] * np.identity(2)))
y_reg = np.dot(X.transpose(), Y)
w = np.dot(w_reg, y_reg)
print("\n")
print("linear regression weights with regularization")
for i in range(0, len(w)):
    print("w%d: " % (i), end="")
    print(w[i])
y_hat_test = 0
for d in range (0, 24):
            if d % 2 == 1:
                y_hat_test += X.item(d)*w[1] + w[0]
y_test_total = np.sum(Y)
ein_test = (y_hat_test - y_test_total)**2
print("Ein for full set with regularization")
print(ein_test/12)
    

axes = plt.gca()
x = np.linspace(-4,12)
plt.scatter(X_scatter, Y)
plt.plot(x,x*linear_weights[1] + linear_weights[0], color = 'black', label = 'no reg')
plt.plot(x,x*w[1] + w[0],color = 'red', label = 'reg', alpha = .5)
plt.legend()
